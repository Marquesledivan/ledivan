#!/home/developer/api-web/venv/bin/python3

from flask import Flask, render_template, request

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/cadastro', methods=["GET", "POST"])
def cadastro():
    if request.method == "GET":
        return render_template('cadastro.html')
    elif request.method == "POST":
        pass
if __name__ == "__main__":
    app.run(debug=True)


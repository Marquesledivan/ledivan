#!/home/developer/prato/venv/bin/python3
from docker.docker import docker
from flask import Flask, render_template

app = Flask(__name__)
app.register_blueprint(docker)
@app.route('/')

def index():
    return render_template('index.html')
if __name__ == "__mainx__":
    app.run(debug=True)
